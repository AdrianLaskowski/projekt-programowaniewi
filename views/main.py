#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import random #umozliwia uzywanie metody random
import sys #do kodowania systemowego utf-8

import webapp2 #uzylem tego srodowiska aby moc wgrac aplikację na serwer google
import jinja2 #do template'ow

#importy klas modeli
from models.budynek import Budynek
from models.kierunek import Kierunek
from models.kierunek import PrzedmiotNaKierunku
from models.pracownik import Pracownik
from models.przedmiot import Przedmiot
from models.student import Student
#inne importy
from time import sleep
from google.appengine.ext import db
from pytz.gae import pytz

#ustawienie domyslnego folderu template'ow (plikow html)
template_dir = os.path.join(os.path.dirname(__file__),'..','templates')
jinja_env = jinja2.Environment(loader = jinja2.FileSystemLoader(template_dir),
                               autoescape = True)
#powyższy autoescape służy jako zabezpiecznie przed różnym formom injection

#ustawienie domyslnego kodowania na utf8
reload(sys)
sys.setdefaultencoding('utf-8')
#umozliwia przekazywanie wartosci do template'ow w postaci argumentow
def render_str(template, **params):
    t = jinja_env.get_template(template)
    return t.render(params)

class BaseHandler(webapp2.RequestHandler):
    #dalej do renderowania stron i przekazywania wartosci do template'ow
    def write(self, *a, **kw):
        self.response.out.write(*a, **kw)

    def render_str(self, template, **params):
        return render_str(template, **params)

    def render(self, template, **kw):
        self.write(self.render_str(template, **kw))

    #wyswietlanie strony startowej
    def get(self):
        self.render('base.html')

class Studenci(BaseHandler):
    def get(self):
        studenci = Student().all()
        self.render('studenci.html', studenci=studenci)

    def post(self):
        student = Student().get_by_id(int(self.request.get('id')))
        db.delete(student)

        sleep(0.1)
        self.redirect('/studenci')

class DodajStudenta(BaseHandler):
    def get(self):
        kierunki = Kierunek().all()
        indeks = random.randint(10000,50000)
        self.render('dodaj-studenta.html', indeks=indeks, kierunki=kierunki)

    def post(self):
        student = Student()

        student.nr_indeksu  = int(self.request.get('nr-indeksu'))
        student.imie = self.request.get('imie')
        student.nazwisko = self.request.get('nazwisko')
        student.rok = int(self.request.get('rok'))
        student.kierunek = [db.Key(str(unicode(self.request.get('kierunek'))))]
        student.put()

        sleep(0.1)
        self.redirect('/studenci')

class EdytujStudenta(BaseHandler):
    def get(self, student_id):
        kierunki = Kierunek().all()
        student = Student().get_by_id(int(student_id))
        self.render('edytuj-studenta.html', student=student, kierunki=kierunki)

    def post(self, student_id):
        student = Student().get_by_id(int(student_id))

        student.nr_indeksu  = int(self.request.get('nr-indeksu'))
        student.imie = self.request.get('imie')
        student.nazwisko = self.request.get('nazwisko')
        student.rok = int(self.request.get('rok'))
        student.kierunek = [db.Key(str(unicode(self.request.get('kierunek'))))]
        student.put()

        sleep(0.1)
        self.redirect('/studenci')

class Kierunki(BaseHandler):
    def get(self):
        kierunki = Kierunek().all()
        przedmioty = PrzedmiotNaKierunku().all()
        self.render('kierunki.html', kierunki=kierunki, przedmioty=przedmioty)

    def post(self):
        kierunek = Kierunek().get_by_id(int(self.request.get('id')))
        studenci = Student().all().fetch(1000)
        przedmioty = PrzedmiotNaKierunku().all()

        # usuwa kierunek z listy kierunkow studenta
        for student in studenci:
            if kierunek.key() in student.kierunek:
                klucz = db.Key(str(unicode(kierunek.key())))
                kierunki_studenta = student.kierunek
                kierunki_studenta.remove(klucz)
                student.kierunek = kierunki_studenta
                db.put(student)
        # usuwa przedmioty na kierunku
        for przedmiot in przedmioty:
            if przedmiot.parent() and przedmiot.parent().key() == kierunek.key():
                db.delete(przedmiot)
        # usuwa kierunek
        db.delete(kierunek)

        sleep(0.1)
        self.redirect('/kierunki')

class DodajKierunek(BaseHandler):
    def get(self):
        przedmioty = Przedmiot().all()
        self.render('dodaj-kierunek.html', przedmioty=przedmioty)

    def post(self):
        kierunek = Kierunek()
        # dodaj nazwę kierunku
        kierunek.nazwa = self.request.get('nazwa')
        kierunek.put()

        ## dodaj przedmioty i mianuj kierunek rodzicem
        lista_nazw_przedmiotow = self.request.get_all('przedmioty')
        lista_lat_przedmiotow = self.request.get_all('rok')
        zipped_list = zip(lista_nazw_przedmiotow, lista_lat_przedmiotow)
        for i in zipped_list:
            przedmiot = PrzedmiotNaKierunku(przedmiot=kierunek,
                            nazwa=i[0],
                            rok=int(i[1]),
                            parent=kierunek).put()

        sleep(0.1)
        self.redirect('/kierunki')

class EdytujKierunek(BaseHandler):
    def get(self, kierunek_id):
        kierunek = Kierunek().get_by_id(int(kierunek_id))
        przedmioty_nk = PrzedmiotNaKierunku().all()
        przedmioty = Przedmiot().all().fetch(100)
        lista_przedmiotow = []

        for p in przedmioty_nk:
            if p.parent().key().id() == kierunek.key().id():
                lista_przedmiotow.append(p)

        lista_pomocnicza = [y.nazwa for y in lista_przedmiotow]

        if not lista_przedmiotow == przedmioty:
            for x in przedmioty:
                print x.nazwa
                if not x.nazwa in lista_pomocnicza:
                    lista_przedmiotow.append(x)

        self.render('edytuj-kierunek.html', kierunek=kierunek, lista_przedmiotow=lista_przedmiotow, przedmioty=przedmioty)

    def post(self, kierunek_id):
        kierunek = Kierunek().get_by_id(int(kierunek_id))
        przedmioty = PrzedmiotNaKierunku().all()


        # usuń przedmioty należące do kierunku
        for przedmiot in przedmioty:
            if przedmiot.parent().key().id() == kierunek.key().id():
                db.delete(przedmiot)

        # update nazwy kierunku
        kierunek.nazwa = self.request.get('nazwa')
        kierunek.put()

        # dodaj zupdateowane przedmioty i mianuj kierunek rodzicem
        lista_nazw_przedmiotow = self.request.get_all('przedmioty')
        lista_lat_przedmiotow = self.request.get_all('rok')
        zipped_list = zip(lista_nazw_przedmiotow, lista_lat_przedmiotow)
        for i in zipped_list:
            przedmiot = PrzedmiotNaKierunku(przedmiot=kierunek,
                            nazwa=i[0],
                            rok=int(i[1]),
                            parent=kierunek).put()

        sleep(0.1)
        self.redirect('/kierunki')


class Przedmioty(BaseHandler):
    def get(self):
        przedmioty = Przedmiot().all()
        self.render('przedmioty.html', przedmioty=przedmioty)

    def post(self):
        przedmiot = Przedmiot().get_by_id(int(self.request.get('id')))
        przedmioty_nk = PrzedmiotNaKierunku().all()
        for p in przedmioty_nk:
            if przedmiot.nazwa == p.nazwa:
                db.delete(p)
        db.delete(przedmiot)

        sleep(0.1)
        self.redirect('/przedmioty')

class DodajPrzedmiot(BaseHandler):
    def get(self):
        pracownicy = Pracownik().all()
        budynki = Budynek().all()
        self.render('dodaj-przedmiot.html', pracownicy=pracownicy, budynki=budynki)

    def post(self):
        przedmiot = Przedmiot()

        przedmiot.nazwa = self.request.get('nazwa')
        nauczyciele = self.request.get_all('nauczyciele')
        key_list = []
        for n in nauczyciele: key_list.append(db.Key(str(unicode(n))))
        przedmiot.nauczyciele = key_list
        # budynek i sala
        budynek_sala = self.request.get('budynek-sala')
        budynek_sala_lista = budynek_sala.split(' : ')
        przedmiot.budynek = budynek_sala_lista[0]
        przedmiot.sala = budynek_sala_lista[1]

        przedmiot.put()

        sleep(0.1)
        self.redirect('/przedmioty')

class EdytujPrzedmiot(BaseHandler):
    def get(self, przedmiot_id):
        przedmiot = Przedmiot().get_by_id(int(przedmiot_id))
        pracownicy = Pracownik().all()
        budynki = Budynek().all()
        self.render('edytuj-przedmiot.html', przedmiot=przedmiot, pracownicy=pracownicy, budynki=budynki)

    def post(self, przedmiot_id):
        przedmiot = Przedmiot().get_by_id(int(przedmiot_id))
        przedmioty = PrzedmiotNaKierunku().all()

        nazwa = self.request.get('nazwa')
        for p in przedmioty:
            if przedmiot.nazwa == p.nazwa:
                p.nazwa = nazwa
            db.put(p)

        przedmiot.nazwa = nazwa
        nauczyciele = self.request.get_all('nauczyciele')
        key_list = []
        for n in nauczyciele: key_list.append(db.Key(str(unicode(n))))
        przedmiot.nauczyciele = key_list
        # budynek i sala
        budynek_sala = self.request.get('budynek-sala')
        budynek_sala_lista = budynek_sala.split(' : ')
        przedmiot.budynek = budynek_sala_lista[0]
        przedmiot.sala = budynek_sala_lista[1]

        przedmiot.put()

        sleep(0.1)
        self.redirect('/przedmioty')

class Pracownicy(BaseHandler):
    def get(self):
        pracownicy = Pracownik().all()
        self.render('pracownicy.html', pracownicy=pracownicy)

    def post(self):
        pracownik = Pracownik().get_by_id(int(self.request.get('id')))
        przedmioty = Przedmiot().all()

        for p in przedmioty:
            if pracownik.key() in p.nauczyciele:
                nauczyciele = p.nauczyciele
                nauczyciele.remove(pracownik.key())
                p.nauczyciele = nauczyciele
                db.put(p)
        db.delete(pracownik)

        sleep(0.1)
        self.redirect('/pracownicy')


class DodajPracownika(BaseHandler):
    def get(self):
        self.render('dodaj-pracownika.html')

    def post(self):
        pracownik = Pracownik()

        pracownik.imie = self.request.get('imie')
        pracownik.nazwisko = self.request.get('nazwisko')
        pracownik.put()

        sleep(0.1)
        self.redirect('/pracownicy')



class EdytujPracownika(BaseHandler):
    def get(self, pracownik_id):
        pracownik = Pracownik().get_by_id(int(pracownik_id))

        self.render('edytuj-pracownika.html', pracownik=pracownik)

    def post(self, pracownik_id):
        pracownik = Pracownik().get_by_id(int(pracownik_id))
        przedmioty = Przedmiot().all()

        imie = self.request.get('imie')
        nazwisko = self.request.get('nazwisko')


        pracownik.imie = imie
        pracownik.nazwisko = nazwisko
        pracownik.put()


        sleep(0.1)
        self.redirect('/pracownicy')

class Budynki(BaseHandler):
    def get(self):
        budynki = Budynek().all()
        self.render('budynki.html', budynki=budynki)

    def post(self):
        budynek = Budynek().get_by_id(int(self.request.get('id')))
        przedmioty = Przedmiot().all()

        for p in przedmioty:
            if budynek.numer == p.budynek:
                p.budynek = 'brak'
                p.sala = 'brak'
                db.put(p)
        db.delete(budynek)

        sleep(0.1)
        self.redirect('/budynki')

class DodajBudynek(BaseHandler):
    def get(self):
        budynki = Budynek().all()
        self.render('dodaj-budynek.html', budynki=budynki)

    def post(self):
        budynek = Budynek()

        budynek.numer = 'B' + self.request.get('numer')
        sale = self.request.get_all('sala')
        lista = []
        for s in sale:
            s = 's' + s
            lista.append(s)
        budynek.sale = lista
        budynek.put()

        sleep(0.1)
        self.redirect('/budynki')

class EdytujBudynek(BaseHandler):
    def get(self, budynek_id):
        budynek = Budynek().get_by_id(int(budynek_id))
        lista_sal = budynek.sale
        sale = []
        for s in lista_sal:
            s = s[1:]
            sale.append(int(s))

        self.render('edytuj-budynek.html', budynek=budynek, sale=sale)

    def post(self, budynek_id):
        budynek = Budynek().get_by_id(int(budynek_id))
        przedmioty = Przedmiot().all().filter('budynek =', budynek.numer)

        numer = 'B' + self.request.get('numer')
        sale = self.request.get_all('sala')
        lista = []
        for s in sale:
            s = 's' + s
            lista.append(s)

        for p in przedmioty:
            if p.budynek == budynek.numer:
                p.budynek = numer
            if not p.sala in lista:
                p.sala = 'brak'
            db.put(p)

        budynek.numer = numer
        budynek.sale = lista
        budynek.put()

        sleep(0.1)
        self.redirect('/budynki')

class SpisZajec(BaseHandler):
    def get(self):
        kierunki = Kierunek().all()
        self.render('spis-zajec.html', kierunki=kierunki)

    def post(self):
        kierunki = Kierunek().all()
        kierunek_id = self.request.get('kierunek-id')

        sleep(0.1)
        self.redirect('/spis-zajec/' + kierunek_id)

class SpisZajecKierunku(BaseHandler):
    def get(self, kierunek_id):
        kierunki = Kierunek().all()
        kierunek = Kierunek().get_by_id(int(kierunek_id))
        przedmioty = PrzedmiotNaKierunku().all()

        lista_przedmiotow = []
        for p in przedmioty:
            if p.parent().key().id() == kierunek.key().id():
                lista_przedmiotow.append(int(p.rok))

        self.render('spis-zajec-kierunku.html', kierunki=kierunki, kierunek=kierunek, lista_przedmiotow=lista_przedmiotow)

    def post(self, kierunek_id):
        kierunek = Kierunek().get_by_id(int(kierunek_id))
        nowy_kierunek_id = self.request.get('kierunek-id')

        sleep(0.1)
        self.redirect('/spis-zajec/' + nowy_kierunek_id)

# [START app]
app = webapp2.WSGIApplication([
    ('/', BaseHandler),
    ('/studenci', Studenci),
    ('/studenci/dodaj-studenta', DodajStudenta),
    ('/studenci/edytuj-studenta/(.*)', EdytujStudenta),
    ('/kierunki', Kierunki),
    ('/kierunki/dodaj-kierunek', DodajKierunek),
    ('/kierunki/edytuj-kierunek/(.*)', EdytujKierunek),
    ('/przedmioty', Przedmioty),
    ('/przedmioty/dodaj-przedmiot', DodajPrzedmiot),
    ('/przedmioty/edytuj-przedmiot/(.*)', EdytujPrzedmiot),
    ('/pracownicy', Pracownicy),
    ('/pracownicy/dodaj-pracownika', DodajPracownika),
    ('/pracownicy/edytuj-pracownika/(.*)', EdytujPracownika),
    ('/spis-zajec', SpisZajec),
    ('/spis-zajec/(.*)', SpisZajecKierunku),
    ('/budynki', Budynki),
    ('/budynki/dodaj-budynek', DodajBudynek),
    ('/budynki/edytuj-budynek/(.*)', EdytujBudynek),

], debug=True)
