from google.appengine.ext import db
from pytz.gae import pytz
from models.przedmiot import Przedmiot

class Kierunek(db.Model):
    nazwa = db.StringProperty()
    data_dodania = db.DateTimeProperty(auto_now_add=True)
    ostatnia_edycja = db.DateTimeProperty(auto_now=True)

    def get_date(self, timezone='Europe/Warsaw'):
        tz = pytz.timezone(timezone)
        auth_date = pytz.utc.localize(self.data_dodania).astimezone(tz)
        return auth_date.strftime('%d-%m-%Y %-H:%M:%S')

    def get_edit_date(self, timezone='Europe/Warsaw'):
        tz = pytz.timezone(timezone)
        auth_date = pytz.utc.localize(self.ostatnia_edycja).astimezone(tz)
        return auth_date.strftime('%d-%m-%Y %-H:%M:%S')

    def get_names(self):
        lista_przedmiotow = self.przedmioty
        lista = []
        for i in lista_przedmiotow:
            lista.append(i.nazwa)
        return ', '.join(lista)

class PrzedmiotNaKierunku(db.Model):
    przedmiot = db.ReferenceProperty(Kierunek, collection_name='przedmioty')
    nazwa = db.StringProperty()
    rok = db.IntegerProperty()

    def get_teachers2(self):
        p = self
        przedmioty = Przedmiot().all()
        for przedmiot in przedmioty:
            if przedmiot.nazwa == p.nazwa:
                return przedmiot.get_teachers()

    def get_budynek(self):
        p = self
        przedmioty = Przedmiot().all()
        for przedmiot in przedmioty:
            if przedmiot.nazwa == p.nazwa:
                return przedmiot.budynek

    def get_sala(self):
        p = self
        przedmioty = Przedmiot().all()
        for przedmiot in przedmioty:
            if przedmiot.nazwa == p.nazwa:
                return przedmiot.sala
