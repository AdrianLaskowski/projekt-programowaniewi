from google.appengine.ext import db
from pytz.gae import pytz

class Pracownik(db.Model):
    imie = db.StringProperty()
    nazwisko = db.StringProperty()
    data_dodania = db.DateTimeProperty(auto_now_add=True)
    ostatnia_edycja = db.DateTimeProperty(auto_now=True)

    def get_date(self, timezone='Europe/Warsaw'):
        tz = pytz.timezone(timezone)
        auth_date = pytz.utc.localize(self.data_dodania).astimezone(tz)
        return auth_date.strftime('%d-%m-%Y %-H:%M:%S')

    def get_edit_date(self, timezone='Europe/Warsaw'):
        tz = pytz.timezone(timezone)
        auth_date = pytz.utc.localize(self.ostatnia_edycja).astimezone(tz)
        return auth_date.strftime('%d-%m-%Y %-H:%M:%S')
