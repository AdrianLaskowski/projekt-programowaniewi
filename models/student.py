from google.appengine.ext import db
from pytz.gae import pytz
from models.kierunek import Kierunek

class Student(db.Model):
    imie = db.StringProperty()
    nazwisko = db.StringProperty()
    nr_indeksu = db.IntegerProperty()
    kierunek = db.ListProperty(db.Key)
    rok = db.IntegerProperty()
    data_dodania = db.DateTimeProperty(auto_now_add=True)
    ostatnia_edycja = db.DateTimeProperty(auto_now=True)

    def get_date(self, timezone='Europe/Warsaw'):
        tz = pytz.timezone(timezone)
        auth_date = pytz.utc.localize(self.data_dodania).astimezone(tz)
        return auth_date.strftime('%d-%m-%Y %-H:%M:%S')

    def get_edit_date(self, timezone='Europe/Warsaw'):
        tz = pytz.timezone(timezone)
        auth_date = pytz.utc.localize(self.ostatnia_edycja).astimezone(tz)
        return auth_date.strftime('%d-%m-%Y %-H:%M:%S')

    def get_major(self):
        lista_kierunkow = self.kierunek
        kierunki = Kierunek().all().fetch(1000)
        lista = []
        if not lista_kierunkow == []:
            for k in kierunki:
                if k.key() in lista_kierunkow:
                    nazwa = [ i[0:3] for i in k.nazwa.split() ]
                    pelna_nazwa = ' '.join(nazwa)
        else:
            pelna_nazwa = '-'
        return pelna_nazwa
