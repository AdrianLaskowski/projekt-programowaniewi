from google.appengine.ext import db
from pytz.gae import pytz
from models.przedmiot import Przedmiot

class Budynek(db.Model):
    numer = db.StringProperty()
    sale = db.StringListProperty()
    data_dodania = db.DateTimeProperty(auto_now_add=True)
    ostatnia_edycja = db.DateTimeProperty(auto_now=True)

    def get_date(self, timezone='Europe/Warsaw'):
        tz = pytz.timezone(timezone)
        auth_date = pytz.utc.localize(self.data_dodania).astimezone(tz)
        return auth_date.strftime('%d-%m-%Y %-H:%M:%S')

    def get_edit_date(self, timezone='Europe/Warsaw'):
        tz = pytz.timezone(timezone)
        auth_date = pytz.utc.localize(self.ostatnia_edycja).astimezone(tz)
        return auth_date.strftime('%d-%m-%Y %-H:%M:%S')

    def get_rooms(self):
        sale = self.sale
        return ', '.join(sale)

    def get_raw_building_number(self):
        num = self.numer
        return int(num[1:])

    def get_raw_room_number(self):
        return int(self[1:])
