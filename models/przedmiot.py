from google.appengine.ext import db
from pytz.gae import pytz
from models.pracownik import Pracownik

class Przedmiot(db.Model):
    nazwa = db.StringProperty()
    nauczyciele = db.ListProperty(db.Key)
    budynek = db.StringProperty()
    sala = db.StringProperty()
    data_dodania = db.DateTimeProperty(auto_now_add=True)
    ostatnia_edycja = db.DateTimeProperty(auto_now=True)

    def get_date(self, timezone='Europe/Warsaw'):
        tz = pytz.timezone(timezone)
        auth_date = pytz.utc.localize(self.data_dodania).astimezone(tz)
        return auth_date.strftime('%d-%m-%Y %-H:%M:%S')

    def get_edit_date(self, timezone='Europe/Warsaw'):
        tz = pytz.timezone(timezone)
        auth_date = pytz.utc.localize(self.ostatnia_edycja).astimezone(tz)
        return auth_date.strftime('%d-%m-%Y %-H:%M:%S')

    def get_teachers(self):
        nauczyciele = self.nauczyciele
        pracownik = Pracownik().all().fetch(1000)
        list = []
        for p in pracownik:
            if p.key() in nauczyciele:
                list.append(p.imie[0] + '. ' + p.nazwisko )
        return ', '.join(list)
