$(document).ready(function() {
    // budynki
    $("#dodajBudynek").click(function(e) {
        $("#budynki").append(
            '<div>' +
            '<input class="form-control" style="margin:5px 0; width:80%; display:inline-block" name="sala" type="number" min="1" max="1000" placeholder="numer..." required>' +
            '<button class="usunBudynek btn btn-xs btn-danger" style="margin:0 5px">X</button>' +
            '</div>'
        );
    });

    $("body").on("click", ".usunBudynek", function(e) {
        $(this).parent("div").remove();
    });
});
